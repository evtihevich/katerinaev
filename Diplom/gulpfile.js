const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('image', function () {
    return gulp.src('src/img/*')
        .pipe(image())
        .pipe(gulp.dest('build/img'));
});

gulp.task('sass', function () {
    return gulp.src('src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
          }))
        .pipe(gulp.dest('build/css'));
});

gulp.task('fonts', function () {
    return gulp.src('src/fonts/*')
        .pipe(gulp.dest('build/fonts'));
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.scss', gulp.parallel('sass'));
    gulp.watch('src/img/*', gulp.parallel('image'));
    gulp.watch('src/fonts/*', gulp.parallel('fonts'));
});

gulp.task('default', gulp.parallel('watch', 'fonts', 'image', 'sass'));

